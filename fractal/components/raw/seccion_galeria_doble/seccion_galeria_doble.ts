export function galeria_doble(flikity){
    if(document.getElementsByClassName('galeria-doble__home').length>0){
        $( document ).ready(function() {
            var flky = new flikity( '.galeria-doble__home', {
                wrapAround: true,
                groupCells: true,
                cellAlign: 'center',
                pageDots: false,
                imagesLoaded: false
            });
        });
    }
    
}