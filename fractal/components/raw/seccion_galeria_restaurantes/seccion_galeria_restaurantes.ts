export function galeria_restaurantes(flikity){
    if(document.getElementsByClassName('galeria-restaurantes__flickity').length>0){
        $( document ).ready(function() {
            var flky_restaurantes = new flikity( '.galeria-restaurantes__flickity', {
                wrapAround: true,
                groupCells: true,
                cellAlign: 'center',
                pageDots: false,
                imagesLoaded: false
            });
        });
    }
}