/**
*   Libraries
 */
import * as $ from 'jquery';
import 'bootstrap';
import "flickity";
var Flickity = require('flickity');
import 'flickity-fullscreen';


/**
 * Styles
 */
import "../scss/index.scss";


/**
 * Modules
 */
import {customJs} from './custom';
import {header} from '../wp/secciones/header/header';
/**
 * Init
 */
customJs(Flickity);
header();


