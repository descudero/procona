export function customJs(Flickity) {
    if(document.querySelector('.lista-productos__listado')){
        var flk = new Flickity('.lista-productos__listado', {
            contain: true,
            pageDots: false,
            groupCells: true,
            adaptiveHeight: true,
            watchCSS: true
        });
    }

    if(document.querySelector('.carousel-contacto')){
        var flk = new Flickity('.carousel-contacto', {
            contain: true,
            pageDots: false,
            groupCells: true,
            adaptiveHeight: true,
            lazyLoad: 1,
        });
    }

    if(document.querySelector('.ficha-producto__imagenes')){
        var flk = new Flickity('.ficha-producto__imagenes', {
            contain: true,
            pageDots: false,
            groupCells: true,
            adaptiveHeight: true,
            lazyLoad: 1,
        });
    }

    
    
}