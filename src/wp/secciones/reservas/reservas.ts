export function motor_reservas(moment) {
	
	/*********************************************************
		[INICIO] VARIABLES DE MOTOR
	*********************************************************/
	
	
	var anchoWindow, hotelCode, zoNe, $idNeoBookings, $zonaNeoBookings;
	
	//IDENTIFICADOR DEL FORMULARIO DE MOTOR
	var form = $('#MADG_motorReservas');

	//BE Object
	var BE = {
	  domain : "https://bookings.arenahotelesfuerteventura.com/",
	  extraParams : ""
	};
	
	var codigoCuponResidente = "RESIDENTE";
	
	/*-- NUMERO MÁXIMO DE ADULTOS --*/
	var maxNumAdults = 8;
	var initialAdults = 2;
	
	/*-- NUMERO MÁXIMO DE NIÑOS --*/
	var maxNumNinos = 2;
	var initialNinos = 0;
	
	/*-- NUMERO MÁXIMO DE BEBES --*/
	var maxNumBebes = 1;
	var initialBebes = 0;
	
	//Global CONSTS SYNTAXSUGAR
	var DAY = 0;
	var MONTH = 1;
	var YEAR = 2;
	
	var IN = 1;
	var OUT = 2;
	
	var LANGUAGE = $('html').attr('lang');
	
	var FROM = true;
	var TO = false;
	
	var LEFT = true;
	var RIGHT = false;
	
	var METHOD = "POST";
    
    var fechaInicio = null;
	var fechaFin = null;
	
	//Objeto De textos
	var textos = {
		adultsMax: '',
		niniosMax: '',
		bebesMax: '',
		entrada: '',
		salida: '',
		noches: '',
		hecho: '',
		ninios: '',
		niniosedades: '',
		adultos: '',
		bebes: '',
		residente: '',
		eligeDestinoHotel: '',
		eligeFecha: '',
		introduceCodigoPromo: '',
		destinos: '',
		hoteles: '',
		buscar: '',
		botonHecho: '',
		fraseResidente: ''
	};
	
	var textosES = {
		adultsMax: 'Máximo '+ maxNumAdults + ' adultos.',
		niniosMax: 'Máximo '+ maxNumNinos + ' niños.',
		bebesMax: 'Máximo '+ maxNumBebes + ' bebés.',
		entrada: 'Llegada',
		salida: 'Salida',
		noches: 'Noches',
		hecho: '¡Hecho!',
		ninios: 'Niños',
		niniosedades: 'De 2 a 12 años',
		adultos: 'Adultos',
		bebes: 'Bebés',
		residente: 'Soy residente',
		eligeDestinoHotel: 'Elige hotel o destino',
		eligeFecha: 'Elige tus fechas de estancia',
		introduceCodigoPromo: 'Código Promo',
		destinos: 'Destinos',
		hoteles: 'Hoteles',
		buscar: 'Buscar',
		botonHecho: '¡Hecho!',
		fraseResidente: 'Si eres residente de las Islas Baleares tendrás automaticamente un 5% de descuento en tu reserva. Deberás identificarte con tu documento de identidad / certificado de residencia en la recepción al momento de hacer Checkin.No acumulable a otras promociones.'
	};
	
	var textosDE = {
		adultsMax: 'Máximo '+ maxNumAdults + ' adultos.',
		niniosMax: 'Máximo '+ maxNumNinos + ' niños.',
		bebesMax: 'Máximo '+ maxNumBebes + ' bebés.',
		entrada: 'Llegada',
		salida: 'Salida',
		noches: 'Noches',
		hecho: '¡Hecho!',
		ninios: 'Niños',
		adultos: 'Adultos',
		bebes: 'Bebés',
		residente: 'Soy Residente',
		eligeDestinoHotel: 'Elige hotel o destino',
		eligeFecha: 'Elige tus fechas de estancia',
		introduceCodigoPromo: 'Introduce tu código'
	};
	
	var textosEN = {
		adultsMax: 'Maximum '+ maxNumAdults + ' adults.',
		niniosMax: 'Maximum '+ maxNumNinos + ' children.',
		bebesMax: 'Maximum '+ maxNumBebes + ' babys.',
		entrada: 'Arrival',
		salida: 'Departure',
		noches: 'Nights',
		hecho: '¡Done!',
		ninios: 'Children',
		niniosedades: '2 to 12 years',
		adultos: 'Adults',
		bebes: 'Babies',
		residente: 'I am Resident',
		eligeDestinoHotel: 'Choose hotel or destination',
		eligeFecha: 'Choose your nights to stay',
		introduceCodigoPromo: 'Promocode',
		destinos: 'Destination',
		hoteles: 'Hotels',
		buscar: 'BOOK NOW',
		botonHecho: '¡Done!',
		fraseResidente: 'If you are a resident of the Balearic Islands you will automatically have a 5% discount on your reservation. You must identify yourself with an I.D / residence certificate on the reception the moment you check-in. Not combinable with other promotions.'
		};
	
	var textosFR = {
		adultsMax: 'Maximum '+ maxNumAdults + ' adultes.',
		niniosMax: 'Maximum '+ maxNumNinos + ' enfants.',
		bebesMax: 'Maximum '+ maxNumBebes + ' bébés.',
		entrada: 'Arrivé',
		salida: 'Sorti',
		noches: 'Nuits',
		hecho: 'Fait!',
		ninios: 'Enfants',
		niniosedades: "Depuis 2 jusqu'à 12 ans",
		adultos: 'Adultes',
		bebes: 'Bébés',
		residente: 'Je suis résident',
		eligeDestinoHotel: "Choisissez l'hôtel ou le destination",
		eligeFecha: 'Choisissez votre dates de séjour',
		introduceCodigoPromo: 'Entrez votre code',
		destinos: 'Destinations',
		hoteles: 'Hôtels',
		buscar: 'Chercher',
		botonHecho: 'Fait!',
		fraseResidente: "Si tu es résident de l'Îles Baleares tu auras automatiquement un 5% de rabais dans ton réservation. Tu devras t'identifier avec un document d'identité / certifique de residence dans la reception au moment du check-in. Non cumulable avec d'autres promotions."
		};
	
	var textosIT = {
		adultsMax: 'Máximo '+ maxNumAdults + ' adultos.',
		niniosMax: 'Máximo '+ maxNumNinos + ' niños.',
		bebesMax: 'Máximo '+ maxNumBebes + ' bebés.',
		entrada: 'Arrivo',
		salida: 'Partenza',
		noches: 'Notti',
		hecho: '¡Done!',
		ninios: 'Bambini',
		niniosedades: 'Da 2 a 12 anni',
		adultos: 'Adulti',
		bebes: 'Babies',
		residente: 'Sono un residente',
		eligeDestinoHotel: "Scegli l'hotel o la destinazione",
		eligeFecha: 'Scegli le tue date de soggiorno',
		introduceCodigoPromo: 'Promocode',
		destinos: 'Destinazione',
		hoteles: 'Hotels',
		buscar: 'PRENOTARE',
		botonHecho: '¡Done!',
		fraseResidente: ''
		};
	
	/*-- [INICIO] Textos del calendario --*/
	/*-- MESES --*/
	var arrayMonthNameESP = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
	var arrayMonthNameCAT = ['Gen','Feb','Març','Abril','Maig','Jun','Jul','Ago','Set','Oct','Nov','Des'];
	var arrayMonthNameDEU = ['Jan','Feb','März','April','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'];
	var arrayMonthNameENG = ['Jan','Feb','March','April','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var arrayMonthNameNED = ['Jan','Feb','Maart','April','Mei','Juni','Juli','Aug','Sep','Okt','Nov','Dec'];
	var arrayMonthNameFRA = ['jan','fév','Mars','avril','Mai','juin','juill','août','sep','oct','nov','déc'];
	var arrayMonthNameITA = ['genn','febb','Marzo','aprile','maggioo','giug','lug','ago','sett','ott','nov','dic'];
	var arrayMonthNameRUS = ['января','Февраль','март','апреля','мая','июнь','июль','август','Сентябрь','октября','ноября','декабрь'];
	var arrayMonthNameCHN = ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'];
	
	/*-- DIAS --*/
	var arrayDayNameESP = ['D', 'L', 'M', 'X', 'J', 'V', 'S'];
	var arrayDayNameRUS = ['воскр', 'понед', 'втор', 'среда', 'четве', 'пятн', 'субб'];
	var arrayDayNameCAT = ['Dium', 'Dill', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'dissabte'];
	var arrayDayNameITA = ['Dom', 'Lun', 'Mar', 'Merc', 'Gio', 'Ven', 'Sab'];
	var arrayDayNameENG = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
	var arrayDayNameNED = ['Zon', 'Maan', 'Dins', 'Woens', 'Donder', 'Vrij', 'Zat'];
	var arrayDayNameDEU = ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'];
	var arrayDayNameFRA = ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'];
	/*-- [FIN] Textos del calendario --*/
	
	
	
	
	
	/*********************************************************
		[FIN] VARIABLES DE MOTOR
	*********************************************************/
	var $selector_adultos_value = '.motor-reservas__form-popup-eligenumeropersonas .numeroadultos';
	var $selector_ninios_value = '.motor-reservas__form-popup-eligenumeropersonas .numeroninios';
	var $selector_bebes_value = '.motor-reservas__form-popup-eligenumeropersonas .numerobebes';
	var	$selector_masadultos = '.motor-reservas__form-popup-eligenumeropersonas .fa-plus.adultos';
	var	$selector_masninios = '.motor-reservas__form-popup-eligenumeropersonas .fa-plus.ninios';
	var	$selector_masbebes = '.motor-reservas__form-popup-eligenumeropersonas .fa-plus.bebes';
	var	$selector_menosadultos = '.motor-reservas__form-popup-eligenumeropersonas .fa-minus.adultos';
	var	$selector_menosninios = '.motor-reservas__form-popup-eligenumeropersonas .fa-minus.ninios';
	var	$selector_menosbebes = '.motor-reservas__form-popup-eligenumeropersonas .fa-minus.bebes';
	var $selector_popups = '.motor-reservas__form-popup';
	var $selector_finseleccionpersonas = '.motor-reservas__form-popup #btnFinalizarSeleccion';
	var $selector_popup_eligenumeropersonas = '#eligenumeropersonas';
	var $selector_submit = '.motor-reservas__form-field--button .submitButton';
	var $selector_texto_personas = '.textoEligeNumeroPersonas';
	
	
	/*********************************************************
		[INICIO] INICIALIZACIÓN DE MASCARA
	*********************************************************/
	
	//Iniciamos variables de texto.
	updateCustomTEXTS();
	$('#eligenumeropersonas p.adultos').text(textos.adultos);
	$('#eligenumeropersonas p.ninios').text(textos.ninios);
	$('#eligenumeropersonas p.bebes').text(textos.bebes);
	$('#eligenumeropersonas p.residente').text(textos.residente);
	$('#eligenumeropersonas #btnFinalizarSeleccion').text(textos.hecho);
	$('.text.eligehotelodestino span').text(textos.eligeDestinoHotel);
	$('.text.eligefechas span').text(textos.eligeFecha);
	$('.text.eligefechas span').text(textos.eligeFecha);
	$('.text.eligecupondescuento').attr("placeholder",textos.introduceCodigoPromo);
	$('.submitButton').text(textos.buscar);
	$('.destinos').text(textos.buscar);
	$('.hoteles').text(textos.buscar);
	
	//Inicializamos inputs del motor.
    $('text--number.entrada').text(textos.entrada);
    $('text--number.salida').text(textos.salida);
	
	$($selector_submit).on('click', function(){
		iniciarBusqueda();
	});
	
	/*********************************************************
		[FIN] INICIALIZACIÓN DE MASCARA
	*********************************************************/
	
	$('.reservas__entrada').daterangepicker(getBaseDRPObject("left", "entrada"), getSelectCallbackIn);
	$('.reservas__salida').daterangepicker(getBaseDRPObject("right", "salida"), getSelectCallbackOut);
	$('.entrada .daterangepicker').prepend('<p class="text textdaterangepicker">'+textos.entrada+'</p>');
	$('.salida .daterangepicker').prepend('<p class="text textdaterangepicker">'+textos.salida+'</p>');
    function getSelectCallbackIn(start, end) { 
        setDateEntrada(start);
        //$('.c-form--input.salida').data('daterangepicker').setMinDate(start.add(3, 'days'));
        $('.reservas__salida').click();
	}
	function getSelectCallbackOut(start, end) { 
        setDateSalida(end);
	}
    

    function setDateEntrada(date){
        setText($('.reservas__entrada span'), formatDateDay(date));
        setText($('.text--date.entrada .anio'), formatDateYear(date));
        setText($('.text--date.entrada .mes'), getMonthArray()[date.format('M')-1]);
		updateFakeDate(FROM, formatDate(date));
    }

    function setDateSalida(date){
        setText($('.reservas__salida span'), formatDateDay(date));
        setText($('.text--date.salida .anio'), formatDateYear(date));
        setText($('.text--date.salida .mes'), getMonthArray()[date.format('M')-1]);
        updateFakeDate(TO, formatDate(date));
    }

    $('#MADG_motorReservas .motor-reservas__form-field .field').not('.daterangepicker').on('click', function(){
		$('.motor-reservas').addClass('top');
		$('.motor-reservas__mascara').addClass('desplegado');	
		$('.motor-reservas__form').addClass('container');
		$('.motor-reservas').removeClass('hideBottomBar');
		muestraYOculta($(this).parent());
    });
    
    $('.motor-reservas__mascara.desplegado').on('click', function(){
	    muestraYOculta($(this).parent());
	});	
    
    $('.motorReservaMobile').on('click', function(){
		$('.motor-reservas').addClass('top');
		$('.motor-reservas__mascara').addClass('desplegado');	
		$('.motor-reservas__form').addClass('container');
    });
    
    
    $('#MADG_motorReservas .closeButton').on('click', function(){
	    muestraYOculta($(this).parent());
		$('.motor-reservas').removeClass('top');
		$('.motor-reservas__mascara').removeClass('desplegado');	
		$('.motor-reservas__form').removeClass('container');
    });
    
    $('#MADG_motorReservas .fa-info').attr('data-content', textos.fraseResidente);
    $('#MADG_motorReservas .fa-info').popover('enable');
    $('#MADG_motorReservas .fa-info').on('click', function(){
	    setTimeout(function(){
			$('#MADG_motorReservas .fa-info').popover('hide');
		},4000);
    });
    
    var $nav = $('.motor-reservas');
    var $win = $(window);
    var winH = $win.height();   // Get the window height.
    var winW = $win.width();   // Get the window height.

    $win.on("scroll", function () {
	    //console.log($(this).scrollTop());
        if ($(this).scrollTop() > 200 && winW > 575 ) {
            mostrarAlTopSinBottom();
        } else if(winW > 575) {
	        if($('.motor-reservas.top.hideBottomBar').length == 1){
		        $('.motor-reservas__mascara').removeClass('desplegado');
				setTimeout(function(){
					$('.motor-reservas').removeClass('top');
					$('.motor-reservas').removeClass('hideBottomBar');
					setTimeout(function(){
						$('.motor-reservas__form').removeClass('container');
					},50);
					
				},100);
				//replegarMotorAlCentro();
	        }
        }
    }).on("resize", function(){ // If the user resizes the window
       winH = $(this).height(); // you'll need the new height value
       winW = $win.width();
    });
    
    $('#MADG_motorReservas #eligehotelodestino li .option').on('click', function(){
		$('.text.eligehotelodestino span').text($(this).text());
		muestraYOculta($('.text.eligehotelodestino'));
		if($(this).attr('hotelCode') && $(this).attr('hotelCode').length){
			$('#MADG_hotelZone').val('');
				
			$('#MADG_motorReservas').attr("action", BE.domain + LANGUAGE.substring(0, 2) + '/step-1?id='+$(this).attr('hotelCode'));
		}else{
			if($(this).attr('hotelZone') && $(this).attr('hotelZone').length){
				$('#MADG_hotelZone').val($(this).attr('hotelZone'));
				$('#MADG_motorReservas').attr("action", BE.domain + LANGUAGE.substring(0, 2) + '/step-1');
			}
		}
		
    });

    function getParametrosDeUrl(){
		var url_string = window.location.href;
		var url = new URL(url_string);
		var hotelCode = url.searchParams.get("hotelcode");
		var codPromo = url.searchParams.get("codpromo");
		var entrada = url.searchParams.get("entrada");
		var salida = url.searchParams.get("salida");
		
		if(entrada){
			//var fechaEntrada = moment(entrada).format('DD-MM-YYYY');
		}	
		if(salida){
			//var fechaEntrada = moment(salida).format('DD-MM-YYYY');
		}
		
		console.log(hotelCode);
		console.log(codPromo);
		console.log(entrada);
		console.log(salida);
    }
    
    function applyHotelCodeToMotor(){
	    if($('#eligehotelodestino').attr("hotelCodeToApply") && $('#eligehotelodestino').attr("hotelCodeToApply").length >0 && $('#eligehotelodestino .option[hotelCode="'+$('#eligehotelodestino').attr("hotelCodeToApply")+'"]') && $('#eligehotelodestino .option[hotelCode="'+$('#eligehotelodestino').attr("hotelCodeToApply")+'"]').length >0){
		    $('.text.eligehotelodestino span').text($('#eligehotelodestino .option[hotelCode="'+$('#eligehotelodestino').attr("hotelCodeToApply")+'"]').text());
			$('#MADG_hotelCode').val($('#eligehotelodestino').attr("hotelCodeToApply"));
			$('#MADG_motorReservas').attr("action", BE.domain + LANGUAGE.substring(0, 2) + '/step-1');
	    }
	    
    }
    
    function mostrarAlTopConBottom(){
	    $('.motor-reservas').addClass('top');
		$('.motor-reservas__mascara').addClass('desplegado');	
		$('.motor-reservas__form').addClass('container');
		muestraYOculta($(this).parent());
    }
    function mostrarAlTopSinBottom(){
	    muestraYOculta($(this).parent());
		$('.motor-reservas').addClass('top');	
		$('.motor-reservas__mascara').addClass('desplegado');	
        $('.motor-reservas').addClass('hideBottomBar');
        $('.motor-reservas__form').addClass('container');
    }
    function replegarMotorAlCentro(){
	    //console.log("Reglegamos al centro");
		$('.motor-reservas').removeClass('top');
		setTimeout(function(){
			$('.motor-reservas__mascara').removeClass('desplegado'); 
		    $('.motor-reservas__form').removeClass('container');

		},200);
	    
    }


    
    $($selector_finseleccionpersonas).on('click', function(){
	    $($selector_popup_eligenumeropersonas).removeClass('show');
    });
   
    
    $($selector_finseleccionpersonas).on('click', function(){
	    $($selector_popup_eligenumeropersonas).removeClass('show');
    });
    
    
    function recalcularEdades(bebes, ninios){
	    var edades = '';
	    for(var x=0 ; x<bebes; x++){
		    if(x!=0){
				edades += ';';    
		    }
		    edades += '0';
	    }
	    for(var x=0 ; x<ninios; x++){
		    if(x!=0 || bebes > 0){
				edades += ';';    
		    }
		    edades += '12';
	    }
	    $('#MADG_edadNinios').val(edades);
    }
    
    
    function muestraYOculta(elemento){
	    if(!elemento.find($('#'+elemento.attr("campo"))).hasClass("show")){
		    $($selector_popups).removeClass('show');
		    $('#'+elemento.attr("campo")).addClass('show');
	    }else{
		    $($selector_popups).removeClass('show');
	    }
	    
    }
    
	
	/*-- BE Logic --*/
	function initDRP(fullLoad) {
	  if($('#booking-form').offset().top <= (($('#section-header').offset().top + 100) + 239) || $(window).width() < 992) {
	    updateBookingEngine("down", fullLoad);
	    $('#nAdultos, #nNinos').removeClass('paRRiba');
	  } else {
	    updateBookingEngine("up", fullLoad);
	    $('#nAdultos, #nNinos').addClass('paRRiba');
	  }
	}
	
	function updateBookingEngine(drop, fullLoad) {
	  for(dateType = 0; dateType < 2; dateType ++){
	    updateDRP(drop);
	    if (fullLoad) {
	      updateFakeDate(dateType, getTodayOrTomorrowDate(dateType));
	      //inyectDatePlaceHolders();
	      $('#booking-form-date-from').val('');
	      $('.today').addClass('start-date');
	      preSelectNextDay();
	    }
	 }
	}
	
	function getTodayOrTomorrowDate(today) {
	  var date = new Date();
	  if (!today) date.setDate(date.getDate() + 1);
	  var year = date.getFullYear();
	  var month = date.getMonth() + 1;
	  var day = date.getDate();
	  return day + "/" + month + "/" + year; 
	}
	
	function updateDRP(drop) {
	  var formDate = $('#booking-form-date-from');
	  
	  formDate.daterangepicker(getBaseDRPObject(drop), getSelectCallback);
	  formDate.on('show.daterangepicker', showCalendar);
	  formDate.on('hide.daterangepicker', hideCalendar); 
	
	}
	
	function setTextoNumeroPersonas(){
		var textoPersonas = '';
		if(parseInt($($selector_adultos_value).text())>0){
			textoPersonas += $($selector_adultos_value).text() + ' ' + textos.adultos;
		}
		if(parseInt($($selector_ninios_value).text())>0){
			if(textoPersonas != ''){
				textoPersonas += '/';
			}
			textoPersonas += $($selector_ninios_value).text() + ' ' + textos.ninios;
		}
		if(parseInt($($selector_bebes_value).text())>0){
			if(textoPersonas != ''){
				textoPersonas += '/';
			}
			textoPersonas += $($selector_bebes_value).text() + ' ' + textos.bebes;
		}
		$($selector_texto_personas).html(textoPersonas);
	}
	
	function getBaseDRPObject(drop, inOut){
        var date = new Date();
        if(fechaInicio && fechaFin){
          return {
              "singleDatePicker": true,
              "opens": drop,
              "autoApply": true,
              "autoUpdateInput": true,
              "showCustomRangeLabel": false,
              "locale": { 
                "format": "DD/MM/YYYY",
                "daysOfWeek": getDayArray(),
                "monthNames": getMonthArray(),
                "firstDay":1
              },
              "minDate": date,
              "drops" : drop,
              "parentEl": $('.c-form--input.'+inOut)
            }
        }else{
            var entrada = moment(date,'DD-MM-YYYY').add(3, 'days');
            var salida = moment(date,'DD-MM-YYYY').add(6, 'days');
            setDateEntrada(entrada);
            setDateSalida(salida);            
            return {
                "singleDatePicker": true,
                "opens": drop,
                "autoApply": true,
                "autoUpdateInput": true,
                "showCustomRangeLabel": false,
                "locale": { 
                    "format": "DD/MM/YYYY",
                    "daysOfWeek": getDayArray(),
                    "monthNames": getMonthArray(),
                    "firstDay":1
                },
                "minDate": date,
                "drops" : drop,
                "parentEl": $('.c-form--input.'+inOut)
            }   
        }
        
      }
	
	
		
	function showPopover(element, text, timelong){
		$(element).attr('data-content', text);
		$(element).popover('show');
		if(!timelong){
			timelong = 1000;
		}
		setTimeout(function(){
			$(element).popover('hide');
		},timelong);
	}
	
	function getSelectCallback(start, end) { 
		//console.log(start);
		setText($('.text.eligefechas span'), formatDate(start)+' / '+formatDate(end) + ' / ' + end.diff(start, 'days') + ' ' + textos.noches);
		updateFakeDate(FROM, formatDate(start));
		updateFakeDate(TO, formatDate(end));
	}
	
    function formatDate(date) { return date.format("DD/MM/YYYY"); }
    
    function formatDateDay(date) { return date.format("DD"); }

    function formatDateYear(date) { return date.format("YYYY"); }

    function formatDateMonth(date) { return date.format("m"); }
	
	function setValue(destino, value) { destino.val(value); }
	
	function setText(destino, value) { destino.text(value); }
	
	function updateFakeDate(dateType, date) {
	  var fakeDate = (dateType == FROM ? 'In' : 'Out');
	  var formDate = (dateType == FROM ? 'from' : 'to');
	  setValues(dateType, date);
	}  
	
	
	//Incluye el valor de las fechas en los input del motor.
	function setValues(dateType, date) {
		if(dateType){
			setValue($('#MADG_fechaIn'), date);	
		}else{
			setValue($('#MADG_fechaOut'), date);	
		}
	}
	
	function splitDate(date) { 
	  if (typeof date === 'undefined' || date === null)
	    return false;
	  else
	    return date.split('/');
	}
	
	function updateKidAdults() {
	  $('.adultooo').text(textos.adult);
	  $('.adultoss').text(textos.adults);
	  $('label[for="children"], .ninoss').text(textos.kids);
	}
	
	
	
	function getDayString(pos) { return getDayArray()[pos]; }
	
	function getMonthString(pos) { return getMonthArray()[pos]; }
	
	function getMonthArray() {
	  if (LANGUAGE === 'es-ES') return arrayMonthNameESP;
	  else if (LANGUAGE === 'en-EN') return arrayMonthNameENG;
	  else if (LANGUAGE === 'de-DE') return arrayMonthNameDEU;
	  else if (LANGUAGE === 'fr-FR') return arrayMonthNameFRA;
	  else if (LANGUAGE === 'it-IT') return arrayMonthNameITA;
	  else return arrayMonthNameENG;
	}
	
	function getDayArray() {
	  if (LANGUAGE === 'es-ES') return arrayDayNameESP;
	  else if (LANGUAGE === 'de-DE') return arrayDayNameDEU;
	  else if (LANGUAGE === 'en-EN') return arrayDayNameENG;
	  else if (LANGUAGE === 'fr-FR') return arrayDayNameFRA;
	  else if (LANGUAGE === 'it-IT') return arrayDayNameITA;
	  else return arrayDayNameENG;
	}
	
	function updateCustomTEXTS() {
		if (LANGUAGE === 'es-ES') textos = textosES;
		else if (LANGUAGE === 'de-DE') textos = textosDE;
		else if (LANGUAGE === 'en-EN') textos = textosEN;
		else if (LANGUAGE === 'fr-FR') textos = textosFR;
		else if (LANGUAGE === 'it-IT') textos = textosIT;
		else if (LANGUAGE === '') textos = textosEN;
		else textos = textosEN;
	}
	
	function scrollTriggered() {
	  if ($('body, html').scrollTop() >  332) {
	  	$('#abretesesamo').addClass('quietoAlTop'); 	
	  } else {
	  	$('#abretesesamo').removeClass('quietoAlTop');  	
	  } 
	
	  if ($('#booking-form').hasClass('fixeddd')) {
	  	$('.daterangepicker').addClass('noSalteslene');
	  } else {
	  	$('.daterangepicker').removeClass('noSalteslene');
	  }
	
	}
	
	
	function posicionarCalendario()
	{
		var y = $(window).scrollTop();  //current y position on the page
		var motor = $('#booking-form').offset().top;
		var calendario = $('.daterangepicker').height();
		var alturaVisible = motor - y - calendario;
	
		/*console.log('posY: ' + y);
		console.log('motor: ' + motor);
		console.log('altura calendario: ' + $('.daterangepicker').height());
		console.log('altura visible: ' + alturaVisible);*/
	
		if (!$('#booking-form').hasClass('fixeddd') && (alturaVisible>=100))
		{
			$('.daterangepicker').css('top', $('#booking-form').offset().top - (70 + $('.daterangepicker').height()));
		}
		else if (!$('#booking-form').hasClass('fixeddd') && (alturaVisible<100))
		{
			$('.daterangepicker').css('top', $('#booking-form').offset().top + 120);
		}
		else if ($('#booking-form').hasClass('fixeddd'))
		{
			$('.daterangepicker').css('position', 'fixed').css('top', '145px');
		}
		
	}
		
	function getTheDate(active) { return $('.daterangepicker .input-mini' + (active ? '.active':':not(.active)')).val(); }
	
	function arrivalHasToBeUpdated(e) { return $('.daterangepicker .left .input-mini.active').length; }
	
	function getDateFrom(leftOrRight) { 
	  var position = leftOrRight ? '.left' : '.right';
	  return $('.daterangepicker ' + position + ' input').val();
	}
	
	
	function iniciarBusqueda(){
		if($('#MADG_fechaIn').val()==''){
			var today = moment();
			setValues(true, formatDate(today.add(IN, 'days')));
			setValues(false, formatDate(today.add(OUT+IN, 'days')));
		}
		if ($('#soyResidente').is(':checked')) {
			$('#soyResidente').prop('disabled', true);
			$('#MADG_promocode').val(codigoCuponResidente);
		}
		//console.log("Iniciada Busqueda");
		if($('#MADG_hotelZone') && $('#MADG_hotelZone').val == ''){
			$('#MADG_hotelZone').remove();
		}else{
			$('#MADG_hotelCode').remove();
		}
		
		if($('#MADG_numNinios') && $('#MADG_numNinios').val == ''){
			$('#MADG_numNinios').remove();
		}
		
		if($('#MADG_numBebes') && $('#MADG_numBebes').val == ''){
			$('#MADG_numBebes').remove();
		}
		
		
		$('#MADG_motorReservas').submit();
	}
    
}