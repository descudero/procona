export function imagen_mas_texto_carousel(flikity){
    if(document.querySelectorAll('.servicios-con-imagen .carousel').length>0){
        $( document ).ready(function() {
            var flky = new flikity( '.servicios-con-imagen .carousel', {
                wrapAround: true,
                groupCells: true,
                cellAlign: 'center',
                pageDots: false,
                imagesLoaded: false,
                lazyLoad: 2
            });
        });
    }
}